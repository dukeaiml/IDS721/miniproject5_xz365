# mini-project5: Serverless Rust Microservice 
> Xingyu, Zhang (NetID: xz365)

## Rust Lambda Functionality
The function named _manager_ is desined to save the information of a person (i.e.{```name```: string, ```age```: u8}) which was passed to the API to the database. In addtion, it will provide a response of what piece of information was added (i.e. {Inserted person record: ```name```, ```age``` years old"}). 
_Code_: [Cargo.toml](./manager/Cargo.toml) | [main.rs](./manager/src/main.rs)

Steps to build this lambda function and deploy it:
```bash
cargo lambda build --release
cargo lambda deploy
```

## Database Integration

### Create a DynamoDB Table
```bash
aws dynamodb create-table \
    --table-name manager \
    --attribute-definitions AttributeName=name,AttributeType=S \
    --key-schema AttributeName=name,KeyType=HASH \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5
```

The detail of the initialed table is shown as below:

![terminal](./create_dynamodb_terminal.png) 

In AWS console ```DynamoDB>Tables```:

![console](./dynamodb_console.png)

### Adding Database Permissions To The Corresponding Role
In ```IAM``` section, attach following permisions to the role which allow the API invoke to access the table created before.

![permissions](./role_permissions.png)

## Service Implementation
### Test On The Lambda Function
In ```Lambda``` section, we can test whether the function is deployed correctly and works well.

![lambda test](./api_deploy.png)

### Add An API Trigger And Test It
First, add a API trigger to the lambda function. Then deploy the API at stage Default and test it in the ```API Gateway``` section.

![API test](./api_test.png)

### Invoke The API Through Terminal And Check The Dynamodb Tables
After verifying the functionality of the API, we can invoke it using __API Endpoint__. To be more specific, in this case, is https://6gxwuvtd9i.execute-api.us-east-1.amazonaws.com/default/manager.

The instruction to invoke in __Windows Command Prompt__:
```bash
curl -X POST "https://6gxwuvtd9i.execute-api.us-east-1.amazonaws.com/default/manager" -H "Content-Type: application/json" -d "{\"age\": 68, \"name\": \"Alex\"}"
```

The response of above command:

![cmd response](./terminal_result.png)

The service works well and all data are stored in the table named ```manager```.

![data](./db_result.png)