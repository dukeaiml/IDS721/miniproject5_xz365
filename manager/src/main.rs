use serde::{Deserialize, Serialize};
use serde_json::{Value, json};
use std::collections::HashMap;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use aws_config::{load_from_env};
use lambda_runtime::{handler_fn, Context, Error};


#[derive(Deserialize, Serialize)]
struct Event {
    name: String,
    age: u8,
}

#[derive(Serialize)]
struct LambdaResponse {
    statusCode: u16,
    headers: HashMap<String, String>,
    body: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(_event: Value, _: Context) -> Result<Value, Error> {
    // Ok(json!({
    //     "statusCode": 200,
    //     "headers": { "Content-Type": "application/json" },
    //     "body": _event,
    // }))
    
    let event: Event = serde_json::from_value(_event.clone())
        .map_err(|e| {
            println!("Error deserializing event to Request: {:?}", e); 
            e
        })?;

    let response = save_person(event.name, event.age).await?;

    Ok(response)
}

async fn save_person(name: String, age: u8) -> Result<Value, Error> {
    let config = load_from_env().await;
    let client = Client::new(&config);
    let table_name = "manager";

    let mut item = HashMap::new();
    item.insert("name".to_string(), AttributeValue::S(name.clone()));
    item.insert("age".to_string(), AttributeValue::N(age.to_string()));

    client.put_item()
        .table_name(table_name)
        .set_item(Some(item))
        .send()
        .await
        .map_err(|e| {
            println!("Error saving person: {:?}", e);
            e
        });

    let message = format!("Inserted person record: {}, {} years old", name, age);
    // Create a JSON value directly for the response
    let response = json!({
        "statusCode": 200,
        "headers": { "Content-Type": "application/json" },
        "body": message,
    });

    Ok(response)
}